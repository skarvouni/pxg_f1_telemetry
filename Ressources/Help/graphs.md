## Help with graph control

### Zoom

- Use the mouse wheel
- On a mac with a trackpad, use a pinch gesture
- Click and hold the mouse left button to draw a rectangle in order to zoom on a specific area 

![Zoom Rectangle](:/help/ZoomRectangle)


### Pan

- Click and hold the mouse middle button to move the view
- You can also move the cursor


### Cursor

- Move the cursor to check the values of each displayed graph at a specific position
- The values are displayed in a table below the graphs
- If the view is zoomed, the graph follow the cursor value

![Cursor Table](:/help/CursorTable)

### Default View

- Come back to the default view by pressing the "Home" button on the toolbar above the graphs

![Home Button](:/help/HomeButton)