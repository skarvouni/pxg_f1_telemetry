#include "Tools.h"

#include <QDateTime>
#include <QIcon>
#include <QPixmap>
#include <QVariant>

QImage Tools::loadImageWithColor(const QString &file, const QColor &color)
{
	auto image = QImage(file);

	if(image.isNull() == false) {

		// Colorize if needed
		if(color.isValid() == true) {
			unsigned char R = color.red();
			unsigned char G = color.green();
			unsigned char B = color.blue();
			image = image.convertToFormat(QImage::Format_ARGB32);
			int pixelsCount = image.width() * image.height();
			unsigned char *imageData = image.bits();
			for(int i = 0; i < pixelsCount; i++) {
				imageData[i * 4 + 2] = R; // Use specified color components
				imageData[i * 4 + 1] = G; // Use specified color components
				imageData[i * 4 + 0] = B; // Use specified color components
										  // imageData[i*4 + 3] = srcAlpha; // Keep alpha value
			}
		}
	}

	return image;
}

QIcon Tools::loadIconWithColor(const QString &file, const QColor &color)
{
	auto pixmap = QPixmap::fromImage(loadImageWithColor(file, color));
	return QIcon(pixmap);
}

bool Tools::compareQVariants(const QVariant &v1, const QVariant &v2)
{
	if(v1.type() != v2.type()) {
		return false;
	}

	if(v1.canConvert(QVariant::DateTime)) {
		return v1.toDateTime() < v2.toDateTime();
	} else if(v1.canConvert(QVariant::Double)) {
		return v1.toDouble() < v2.toDouble();
	} else if(v1.canConvert(QVariant::String)) {
		return v1.toString() < v2.toString();
	}

	return false;
}
