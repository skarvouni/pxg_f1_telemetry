#ifndef TRACKLINEBUILDERDIALOG_H
#define TRACKLINEBUILDERDIALOG_H

#include <QButtonGroup>
#include <QDialog>
#include <QHash>

class QAbstractButton;

namespace Ui
{
class TrackLineBuilderDialog;
}

class TrackLineBuilderDialog : public QDialog
{
	Q_OBJECT

  public:
	explicit TrackLineBuilderDialog(QWidget *parent = nullptr);
	~TrackLineBuilderDialog() override;

	void accept() override;

  private:
	Ui::TrackLineBuilderDialog *ui;
	QHash<QAbstractButton *, double> defaultOffsets;
	QButtonGroup *rbButtonGroup;

  private slots:
	void browse();
	void buttonChanged(QAbstractButton *button);
};

#endif // TRACKLINEBUILDERDIALOG_H
