    #-------------------------------------------------
#
# Project created by QtCreator 2018-10-21T14:38:28
#
#-------------------------------------------------

QT       += charts core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = "PXG F1 Telemetry"
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

HEADERS += \
    AboutDialog.h \
    ApplicationSettings.h \
    ButtonTesterDialog.h \
    CheckUpdatesDialog.h \
    CompareLapsWidget.h \
    CompareRaceWidget.h \
    CompareStintsWidget.h \
    CompareTelemetryPage.h \
    CompareTelemetryWidget.h \
    CompareWidgetInterface.h \
    CustomTheme.h \
    F1Telemetry.h \
    F1TelemetrySettings.h \
    FileDownloader.h \
    MeteoTableModel.h \
    PreferencesDialog.h \
    SideToolBar.h \
    TelemetryChart.h \
    TelemetryChartView.h \
    TelemetryDataTableModel.h \
    TelemetryTableWidget.h \
    ThemeDialog.h \
    Tools.h \
    TrackLineBuilderDialog.h \
    TrackView.h \
    TrackingWidget.h

FORMS += \
    AboutDialog.ui \
    ButtonTesterDialog.ui \
    CheckUpdatesDialog.ui \
    CompareTelemetryWidget.ui \
    CustomThemeWidget.ui \
    F1Telemetry.ui \
    PreferencesDialog.ui \
    TelemetryTableWidget.ui \
    ThemeDialog.ui \
    TrackLineBuilderDialog.ui \
    TrackingWidget.ui

SOURCES += \
    AboutDialog.cpp \
    ApplicationSettings.cpp \
    ButtonTesterDialog.cpp \
    CheckUpdatesDialog.cpp \
    CompareLapsWidget.cpp \
    CompareRaceWidget.cpp \
    CompareStintsWidget.cpp \
    CompareTelemetryPage.cpp \
    CompareTelemetryWidget.cpp \
    CustomTheme.cpp \
    F1Telemetry.cpp \
    F1TelemetrySettings.cpp \
    FileDownloader.cpp \
    MeteoTableModel.cpp \
    PreferencesDialog.cpp \
    SideToolBar.cpp \
    TelemetryChart.cpp \
    TelemetryChartView.cpp \
    TelemetryDataTableModel.cpp \
    TelemetryTableWidget.cpp \
    ThemeDialog.cpp \
    Tools.cpp \
    TrackLineBuilderDialog.cpp \
    TrackView.cpp \
    TrackingWidget.cpp \
    main.cpp

RESOURCES += \
    F1Telemetry.qrc

ICON = ../Ressources/F1Telemetry.icns


# Core library
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Core/release/ -lCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Core/debug/ -lCore
else:unix: LIBS += -L$$OUT_PWD/../Core/ -lCore

INCLUDEPATH += $$PWD/..
DEPENDPATH += $$PWD/..

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Core/release/libCore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Core/debug/libCore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Core/release/Core.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Core/debug/Core.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Core/libCore.a


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

