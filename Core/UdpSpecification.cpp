#include "UdpSpecification.h"

#include <QtDebug>
#include <TrackInfo.h>


template <typename T> void readDataList(QDataStream &in, QVector<T> &list, int nb = 22)
{
	list.reserve(nb);
	for(auto i = 0; i < nb; ++i) {
		T data;
		in >> data;
		list.append(data);
	}
}

int UdpSpecification::expectedPacketLength(UdpSpecification::PacketType type) const
{
	return packetExpectedLengths[type];
}

QString UdpSpecification::team(int index) const
{
	if(index == 255) {
		return _myTeamName;
	}
	return teams.value(index);
}

UdpSpecification::UdpSpecification()
{
	packetExpectedLengths[PacketType::Header] = 24;
	packetExpectedLengths[PacketType::Motion] = 1440;
	packetExpectedLengths[PacketType::Session] = 227;
	packetExpectedLengths[PacketType::LapData] = 1166;
	packetExpectedLengths[PacketType::Event] = 11;
	packetExpectedLengths[PacketType::Participants] = 1189;
	packetExpectedLengths[PacketType::CarSetup] = 1078;
	packetExpectedLengths[PacketType::CarTelemetry] = 1283;
	packetExpectedLengths[PacketType::CarStatus] = 1320;
	packetExpectedLengths[PacketType::FinalClassification] = 815;
	packetExpectedLengths[PacketType::LobbyInfo] = 1145;

	teams = QStringList({
		"Mercedes",
		"Ferrari",
		"Red Bull",
		"Williams",
		"Racing Point",
		"Renault",
		"Alpha Tauri",
		"Haas",
		"McLaren",
		"Alfa Romeo", // 9
		"McLaren 1988",
		"McLaren 1991",
		"Williams 1992",
		"Ferrari 1995",
		"Williams 1996",
		"McLaren 1998",
		"Ferrari 2002",
		"Ferrari 2004", // 17
		"Renault 2006",
		"Ferrari 2007",
		"McLaren 2008",
		"Red Bull 2010",
		"Ferrari 1976", // 22
		"ART Grand Prix",
		"Campos Vexatec Racing",
		"Carlin",
		"Charouz Racing System",
		"DAMS",
		"Russian Time",
		"MP Motorsport",
		"Pertamina", // 30
		"Mclaren 1990",
		"Trident",
		"BWT Arden",
		"McLaren 1976",
		"Lotus 1972",
		"Ferrari 1979", // 36
		"McLaren 1982",
		"Williams 2003",
		"Brawn 2009",
		"Lotus 1978", // 40
		"F1 Generic Car",
		"Art GP",
		"Campos",
		"Carlin",
		"Sauber Junior Charouz",
		"Dams",
		"Uni-Virtuosi",
		"MP Motorsport",
		"Prema",
		"Trident",
		"Arden", // 51
		"",
		"Benetton 94",
		"Benetton 95",
		"Ferrari 2000",
		"Jordan 91", // 56
	});
	tracksInfo = QStringList({":/tracks/Melbourne",
							  ":/tracks/Paul Ricard",
							  ":/tracks/Shanghai",
							  ":/tracks/Sakir",
							  ":/tracks/Catalunya",
							  ":/tracks/Monaco",
							  ":/tracks/Montreal",
							  ":/tracks/Silverstone",
							  ":/tracks/Hockenheim",
							  ":/tracks/Hungaroring",
							  ":/tracks/Spa Francorchamp",
							  ":/tracks/Monza",
							  ":/tracks/Singapore",
							  ":/tracks/Suzuka",
							  ":/tracks/Abu Dhabi",
							  ":/tracks/Austin",
							  ":/tracks/Interlagos",
							  ":/tracks/Red Bull Ring",
							  ":/tracks/Sochi",
							  ":/tracks/Mexico",
							  ":/tracks/Baku",
							  ":/tracks/Sakir Short",
							  ":/tracks/Silverstone Short",
							  ":/tracks/Austin Short",
							  ":/tracks/Suzuka Short",
							  ":/tracks/Hanoi",
							  ":/tracks/Zandvoort"});

	sessions = QStringList(
		{"Unknown", "FP1", "FP2", "FP3", "Short FP", "Q1", "Q2", "Q3", "Short Q", "OSQ", "R1", "R2", "Time Trial"});
	tyres = QStringList({"Hyper Soft",
						 "Ultra Soft",
						 "Super Soft",
						 "Soft",
						 "Medium",
						 "Hard",
						 "Super Hard",
						 "Inter",
						 "Full Wet",
						 "Dry (Classic)",
						 "Wet (Classic)",
						 "Super Soft (F2)",
						 "Soft (F2)",
						 "Medium (F2)",
						 "Hard (F2)",
						 "Wet (F2)",
						 "C5",
						 "C4",
						 "C3",
						 "C2",
						 "C1"});

	tyresWorkingRange = QStringList({"85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C"
									 "70°C - 85°C",
									 "60°C - 75°C",
									 "",
									 "",
									 "",
									 "",
									 "",
									 "",
									 "",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C",
									 "85°C - 100°C"});
	visualTyres =
		QStringList({"Hyper Soft", "Ultra Soft",      "Super Soft", "Soft",          "Medium",        "Hard",
					 "Super Hard", "Inter",           "Full Wet",   "Dry (Classic)", "Wet (Classic)", "Super Soft (F2)",
					 "Soft (F2)",  "Medium (F2)",     "Hard (F2)",  "Wet (F2)",      "Soft",          "Medium",
					 "Hard",       "Super Soft (F2)", "Soft (F2)",  "Medium (F2)",   "Hard (F2)",     "Super Soft (F2)",
					 "Soft (F2)",  "Medium (F2)",     "Hard (F2)"});
	ersModesValues = QStringList({"None", "Medium", "Overtake", "Hotlap"});
	fuelMixesValues = QStringList({"Lean", "Standard", "Rich", "Max"});
	weathersValues = QStringList({"Clear", "Cloud", "Overcast", "Light Rain", "Heavy Rain", "Storm"});

	formulaTypes = QStringList({"F1", "F1 Classic", "F2", "F1 Generic"});
	surfaces = QStringList({"Tarmac", "Rumble strip", "Concrete", "Rock", "Gravel", "Mud", "Sand", "Grass", "Water",
							"Cobblestone", "Metal", "Rigged"});
	raceStatusValues =
		QStringList({"Invalid", "Inactive", "Active", "Finished", "Disqualified", "Not Classified", "Retired"});

	buttonNamesPs4 = {{Button::NoButton, "None"},
					  {Button::CrossA, "X"},
					  {Button::TriangleY, "△"},
					  {Button::CircleB, "◯"},
					  {Button::SquareX, "▢"},
					  {Button::Left, "Left"},
					  {Button::Right, "Right"},
					  {Button::Up, "Up"},
					  {Button::Down, "Down"},
					  {Button::OptionMenu, "Option"},
					  {Button::L1, "L1"},
					  {Button::R1, "R1"},
					  {Button::L2, "L2"},
					  {Button::R2, "R2"},
					  {Button::LeftStickClick, "L3"},
					  {Button::RightStickCLick, "R3"}};

	buttonNamesXbox = {{Button::NoButton, "None"},
					   {Button::CrossA, "A"},
					   {Button::TriangleY, "Y"},
					   {Button::CircleB, "B"},
					   {Button::SquareX, "X"},
					   {Button::Left, "Left"},
					   {Button::Right, "Right"},
					   {Button::Up, "Up"},
					   {Button::Down, "Down"},
					   {Button::OptionMenu, "Menu"},
					   {Button::L1, "LB"},
					   {Button::R1, "RB"},
					   {Button::L2, "LT"},
					   {Button::R2, "RT"},
					   {Button::LeftStickClick, "Left Stick Click"},
					   {Button::RightStickCLick, "Right Stick Click"}};
}

QString UdpSpecification::description(const QStringList &data) const
{
	QString desc;
	int index = 0;
	for(const auto &text : data) {
		if(!desc.isEmpty()) {
			desc += ", ";
		}
		desc += QString::number(index);
		desc += ": ";
		desc += text;

		++index;
	}

	return desc;
}

QString UdpSpecification::myTeamName() const { return _myTeamName; }

void UdpSpecification::setMyTeamName(const QString &myTeamName) { _myTeamName = myTeamName; }

std::shared_ptr<TrackInfo> UdpSpecification::trackInfo(int trackIndex, bool useCache)
{
	if(useCache && _loadedTrackInfo.contains(trackIndex)) {
		return _loadedTrackInfo[trackIndex];
	}

	auto track = std::make_shared<TrackInfo>();
	bool ok = track->loadFromFile(tracksInfo.value(trackIndex));

	if(ok && useCache) {
		_loadedTrackInfo[trackIndex] = track;
	}

	return track;
}

QDataStream &operator>>(QDataStream &in, PacketHeader &packet)
{
	in >> packet.m_packetFormat >> packet.m_gameMajorVersion >> packet.m_gameMinorVersion >> packet.m_packetVersion >>
		packet.m_packetId >> packet.m_sessionUID >> packet.m_sessionTime >> packet.m_frameIdentifier >>
		packet.m_playerCarIndex >> packet.m_secondaryPlayerCarIndex;

	return in;
}

QDataStream &operator>>(QDataStream &in, ParticipantData &packet)
{
	in >> packet.m_aiControlled >> packet.m_driverId >> packet.m_teamId >> packet.m_raceNumber >> packet.m_nationality;
	packet.m_name.clear();
	char name[48];
	for(auto i = 0; i < 48; ++i) {
		qint8 c;
		in >> c;
		name[i] = c;
	}

	packet.m_name = QString::fromUtf8(name);
	in >> packet.m_yourTelemetry;

	return in;
}

QDataStream &operator<<(QDataStream &out, const ParticipantData &packet)
{
	out << packet.m_aiControlled << packet.m_driverId << packet.m_teamId << packet.m_raceNumber << packet.m_nationality;
	auto codedName = packet.m_name.toUtf8();
	for(auto i = 0; i < 48; ++i) {
		if(i < codedName.count()) {
			out << quint8(codedName[i]);
		} else {
			out << quint8(0);
		}
	}

	out << packet.m_yourTelemetry;

	return out;
}

QDataStream &operator>>(QDataStream &in, PacketParticipantsData &packet)
{
	in >> packet.m_numActiveCars;
	readDataList<ParticipantData>(in, packet.m_participants);

	// Clean driver names to avoid duplications
	//	int index = 1;
	//	for(auto &driver : packet.m_participants) {
	//		if(driver.m_name == "Player") {
	//			driver.m_name.append("_").append(QString::number(index));
	//		}
	//		++index;
	//	}

	return in;
}


QString ParticipantData::driverFullName() const
{
	if(m_name.isEmpty())
		return QString();
	QString name;
	name += "(";
	name += QString::number(m_raceNumber);
	name += ") ";
	name += m_name;
	auto team = UdpSpecification::instance()->team(m_teamId);
	if(!m_name.isEmpty() && !team.isEmpty()) {
		name += " ";
		name += team;
	}

	return name;
}

QStringList PacketParticipantsData::availableDrivers() const
{
	QStringList names;
	for(auto &driver : m_participants) {
		names << driver.driverFullName();
	}

	return names;
}

QDataStream &operator>>(QDataStream &in, LapData &packet)
{
	in >> packet.m_lastLapTime >> packet.m_currentLapTime >> packet.m_sector1TimeInMS >> packet.m_sector2TimeInMS >>
		packet.m_bestLapTime >> packet.m_bestLapNum >> packet.m_bestLapSector1TimeInMS >>
		packet.m_bestLapSector2TimeInMS >> packet.m_bestLapSector3TimeInMS >> packet.m_bestOverallSector1TimeInMS >>
		packet.m_bestOverallSector1LapNum >> packet.m_bestOverallSector2TimeInMS >> packet.m_bestOverallSector2LapNum >>
		packet.m_bestOverallSector3TimeInMS >> packet.m_bestOverallSector3LapNum >> packet.m_lapDistance >>
		packet.m_totalDistance >> packet.m_safetyCarDelta >> packet.m_carPosition >> packet.m_currentLapNum >>
		packet.m_pitStatus >> packet.m_sector >> packet.m_currentLapInvalid >> packet.m_penalties >>
		packet.m_gridPosition >> packet.m_driverStatus >> packet.m_resultStatus;

	return in;
}

QDataStream &operator>>(QDataStream &in, PacketLapData &packet)
{
	readDataList<LapData>(in, packet.m_lapData);
	return in;
}

QDataStream &operator>>(QDataStream &in, CarTelemetryData &packet)
{
	in >> packet.m_speed >> packet.m_throttle >> packet.m_steer >> packet.m_brake >> packet.m_clutch >> packet.m_gear >>
		packet.m_engineRPM >> packet.m_drs >> packet.m_revLightsPercent;
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_brakesTemperature[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_tyresSurfaceTemperature[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_tyresInnerTemperature[i];
	in >> packet.m_engineTemperature;
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_tyresPressure[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_surfaceType[i];

	return in;
}

QDataStream &operator>>(QDataStream &in, PacketCarTelemetryData &packet)
{
	readDataList<CarTelemetryData>(in, packet.m_carTelemetryData);
	in >> packet.m_buttonStatus >> packet.m_mfdPanelIndex >> packet.m_mfdPanelIndexSecondaryPlayer >>
		packet.m_suggestedGear;
	return in;
}

QDataStream &operator>>(QDataStream &in, CarSetupData &packet)
{
	in >> packet.m_frontWing >> packet.m_rearWing >> packet.m_onThrottle >> packet.m_offThrottle >>
		packet.m_frontCamber >> packet.m_rearCamber >> packet.m_frontToe >> packet.m_rearToe >>
		packet.m_frontSuspension >> packet.m_rearSuspension >> packet.m_frontAntiRollBar >> packet.m_rearAntiRollBar >>
		packet.m_frontSuspensionHeight >> packet.m_rearSuspensionHeight >> packet.m_brakePressure >>
		packet.m_brakeBias >> packet.m_rearLeftTyrePressure >> packet.m_rearRightTyrePressure >>
		packet.m_frontLeftTyrePressure >> packet.m_frontRightTyrePressure >> packet.m_ballast >> packet.m_fuelLoad;
	return in;
}

QDataStream &operator<<(QDataStream &out, const CarSetupData &packet)
{
	out << packet.m_frontWing << packet.m_rearWing << packet.m_onThrottle << packet.m_offThrottle
		<< packet.m_frontCamber << packet.m_rearCamber << packet.m_frontToe << packet.m_rearToe
		<< packet.m_frontSuspension << packet.m_rearSuspension << packet.m_frontAntiRollBar << packet.m_rearAntiRollBar
		<< packet.m_frontSuspensionHeight << packet.m_rearSuspensionHeight << packet.m_brakePressure
		<< packet.m_brakeBias << packet.m_rearLeftTyrePressure << packet.m_rearRightTyrePressure
		<< packet.m_frontLeftTyrePressure << packet.m_frontRightTyrePressure << packet.m_ballast << packet.m_fuelLoad;
	return out;
}

QDataStream &operator>>(QDataStream &in, PacketCarSetupData &packet)
{
	readDataList<CarSetupData>(in, packet.m_carSetups);
	return in;
}


QDataStream &operator>>(QDataStream &in, MarshalZone &packet)
{
	in >> packet.m_zoneStart >> packet.m_zoneFlag;
	return in;
}

QDataStream &operator>>(QDataStream &in, WeatherForecastSample &packet)
{
	in >> packet.m_sessionType >> packet.m_timeOffset >> packet.m_weather >> packet.m_trackTemperature >>
		packet.m_airTemperature;

	return in;
}


QDataStream &operator>>(QDataStream &in, PacketSessionData &packet)
{
	in >> packet.m_weather >> packet.m_trackTemperature >> packet.m_airTemperature >> packet.m_totalLaps >>
		packet.m_trackLength >> packet.m_sessionType >> packet.m_trackId >> packet.m_formula >>
		packet.m_sessionTimeLeft >> packet.m_sessionDuration >> packet.m_pitSpeedLimit >> packet.m_gamePaused >>
		packet.m_isSpectating >> packet.m_spectatorCarIndex >> packet.m_sliProNativeSupport >> packet.m_numMarshalZones;
	readDataList<MarshalZone>(in, packet.m_marshalZones, 21);
	in >> packet.m_safetyCarStatus >> packet.m_networkGame >> packet.m_numWeatherForecastSamples;
	readDataList<WeatherForecastSample>(in, packet.m_weatherForecastSamples, 20);
	return in;
}

QDataStream &operator>>(QDataStream &in, CarStatusData &packet)
{
	in >> packet.m_tractionControl >> packet.m_antiLockBrakes >> packet.m_fuelMix >> packet.m_frontBrakeBias >>
		packet.m_pitLimiterStatus >> packet.m_fuelInTank >> packet.m_fuelCapacity >> packet.m_fuelRemainingLaps >>
		packet.m_maxRPM >> packet.m_idleRPM >> packet.m_maxGears >> packet.m_drsAllowed >>
		packet.m_drsActivationDistance;
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_tyresWear[i];
	in >> packet.m_tyreCompound >> packet.m_tyreVisualCompound >> packet.m_tyresAgeLaps;
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_tyresDamage[i];
	in >> packet.m_frontLeftWingDamage >> packet.m_frontRightWingDamage >> packet.m_rearWingDamage >>
		packet.m_drsFault >> packet.m_engineDamage >> packet.m_gearBoxDamage >> packet.m_vehicleFiaFlags >>
		packet.m_ersStoreEnergy >> packet.m_ersDeployMode >> packet.m_ersHarvestedThisLapMGUK >>
		packet.m_ersHarvestedThisLapMGUH >> packet.m_ersDeployedThisLap;
	return in;
}

QDataStream &operator>>(QDataStream &in, PacketCarStatusData &packet)
{
	readDataList<CarStatusData>(in, packet.m_carStatusData);
	return in;
}

QDataStream &operator>>(QDataStream &in, PacketMotionData &packet)
{
	readDataList<CarMotionData>(in, packet.m_carMotionData);

	for(auto i = 0; i < 4; ++i)
		in >> packet.m_suspensionPosition[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_suspensionVelocity[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_suspensionAcceleration[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_wheelSpeed[i];
	for(auto i = 0; i < 4; ++i)
		in >> packet.m_wheelSlip[i];

	in >> packet.m_localVelocityX >> packet.m_localVelocityY >> packet.m_localVelocityZ >> packet.m_angularVelocityX >>
		packet.m_angularVelocityY >> packet.m_angularVelocityZ >> packet.m_angularAccelerationX >>
		packet.m_angularAccelerationY >> packet.m_angularAccelerationZ >> packet.m_frontWheelsAngle;

	return in;
}

QDataStream &operator>>(QDataStream &in, CarMotionData &packet)
{
	in >> packet.m_worldPositionX >> packet.m_worldPositionY >> packet.m_worldPositionZ >> packet.m_worldVelocityX >>
		packet.m_worldVelocityY >> packet.m_worldVelocityZ >> packet.m_worldForwardDirX >> packet.m_worldForwardDirY >>
		packet.m_worldForwardDirZ >> packet.m_worldRightDirX >> packet.m_worldRightDirY >> packet.m_worldRightDirZ >>
		packet.m_gForceLateral >> packet.m_gForceLongitudinal >> packet.m_gForceVertical >> packet.m_yaw >>
		packet.m_pitch >> packet.m_pitch;

	return in;
}

QDataStream &operator>>(QDataStream &in, PacketEventData &packet)
{
	packet.m_eventStringCode.clear();
	char name[4];
	for(auto i = 0; i < 4; ++i) {
		qint8 c;
		in >> c;
		name[i] = c;
	}

	packet.m_eventStringCode = QString::fromUtf8(name);
	in >> packet.details1 >> packet.details2 >> packet.details3 >> packet.details4;
	packet.event = stringToEvent(packet.m_eventStringCode);
	return in;
}

Event stringToEvent(const QString str)
{
	if(str.startsWith("SSTA"))
		return Event::SessionStarted;
	if(str.startsWith("SEND"))
		return Event::SessionEnded;
	if(str.startsWith("FTLP"))
		return Event::FastestLap;
	if(str.startsWith("RTMT"))
		return Event::Retirement;
	if(str.startsWith("DRSE"))
		return Event::DrsEnabled;
	if(str.startsWith("DRSD"))
		return Event::DrsDisabled;
	if(str.startsWith("TMPT"))
		return Event::TeammateInPits;
	if(str.startsWith("CHQF"))
		return Event::ChequeredFlag;
	if(str.startsWith("RCWN"))
		return Event::RaceWinner;
	if(str.startsWith("PENA"))
		return Event::PenaltyIssued;
	//	if(str.startsWith("SPTP"))
	//		return Event::SpeedTrapTrigged;

	return Event::Unknown;
}

bool PacketSessionData::isRace() const { return m_sessionType == 10 || m_sessionType == 11; }

QString PacketSessionData::sessionName() const
{
	auto trackName = TRACK_INFO(m_trackId)->name;
	auto sessionType = UdpSpecification::instance()->session_type(m_sessionType);

	return trackName + " " + sessionType;
}

QVector<WeatherForecastSample> PacketSessionData::currentSessionWeatherForecastSamples() const
{
	QVector<WeatherForecastSample> results;
	std::copy_if(m_weatherForecastSamples.begin(), m_weatherForecastSamples.end(), std::back_inserter(results),
				 [this](auto forecast) { return forecast.m_sessionType == m_sessionType; });

	return results;
}

QDataStream &operator>>(QDataStream &in, FinalClassificationData &packet)
{
	in >> packet.m_position >> packet.m_numLaps >> packet.m_gridPosition >> packet.m_points >> packet.m_numPitStops >>
		packet.m_resultStatus >> packet.m_bestLapTime >> packet.m_totalRaceTime >> packet.m_totalRaceTime2 >>
		packet.m_penaltiesTime >> packet.m_numPenalties >> packet.m_numTyreStints;
	readDataList<quint8>(in, packet.m_tyreStintsActual, 8);
	readDataList<quint8>(in, packet.m_tyreStintsVisual, 8);
	return in;
}

QDataStream &operator>>(QDataStream &in, PacketFinalClassificationData &packet)
{
	in >> packet.m_numCars;
	readDataList<FinalClassificationData>(in, packet.m_classificationData);
	return in;
}

int PacketLapData::lapIndexForCarPosition(int pos) const
{
	int index = 0;
	for(auto &lap : m_lapData) {
		if(lap.m_carPosition == pos) {
			return index;
		}

		++index;
	}

	return -1;
}

bool WeatherForecastSample::operator==(const WeatherForecastSample &other) const
{
	return m_sessionType == other.m_sessionType && m_airTemperature == other.m_airTemperature &&
		   m_timeOffset == other.m_timeOffset && m_trackTemperature == other.m_trackTemperature &&
		   m_weather == other.m_weather;
}
