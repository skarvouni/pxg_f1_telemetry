#ifndef BUTTONSMANAGER_H
#define BUTTONSMANAGER_H

#include "UdpSpecification.h"

#include <QHash>


class ButtonsManager
{
  public:
	ButtonsManager();

	void setButtonsStatus(int status);

	void trackButton(Button button);
	void untrackButton(Button button);
	void clearTrackedButtons();

	void setPressed_callback(const std::function<void(Button)> &callback);
	void setReleased_callback(const std::function<void(Button)> &callback);

  private:
	std::function<void(Button)> _pressed_func;
	std::function<void(Button)> _released_func;
	QHash<int, bool> _pressed;
};

#endif // BUTTONSMANAGER_H
