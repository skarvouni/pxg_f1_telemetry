#include "ModeData.h"

#include <algorithm>

ModeData::ModeData() { clear(); }

void ModeData::addValue(int mode, double lapDistance)
{
	if(!_modeStartDistances.isEmpty() && lapDistance < _modeStartDistances.last().second) {
		removeDataFromDistance(lapDistance);
	}

	if(_modeStartDistances.isEmpty() || _modeStartDistances.last().first != mode) {
		_modeStartDistances << qMakePair(mode, lapDistance);
	}
}

void ModeData::finalize(double lapDistance)
{
	addValue(-1, lapDistance);
	compute();
}

void ModeData::clear()
{
	_modeStartDistances.clear();
	distancesPerMode.clear();
}

ModeData &ModeData::operator+=(const ModeData &other)
{
	for(auto it = other.distancesPerMode.cbegin(); it != other.distancesPerMode.cend(); ++it) {
		if(distancesPerMode.contains(it.key())) {
			distancesPerMode[it.key()] += other.distancesPerMode[it.key()];
		} else {
			distancesPerMode[it.key()] = other.distancesPerMode[it.key()];
		}
	}

	return *this;
}

void ModeData::removeDataFromDistance(double distance)
{
	auto it = std::find_if(_modeStartDistances.begin(), _modeStartDistances.end(),
						   [distance](auto value) { return value.second > distance; });
	if(it != _modeStartDistances.end()) {
		_modeStartDistances.erase(it, _modeStartDistances.end());
	}
}

void ModeData::compute()
{
	distancesPerMode.clear();
	if(_modeStartDistances.count() < 2) {
		return;
	}

	for(auto it = _modeStartDistances.cbegin(); it != _modeStartDistances.cend() - 1; ++it) {

		auto mode = it->first;
		if(!distancesPerMode.contains(mode)) {
			distancesPerMode[mode] = 0;
		}

		auto nextIt = it + 1;
		distancesPerMode[mode] += (nextIt->second - it->second);
	}
}

QDataStream &operator<<(QDataStream &out, const ModeData &data)
{
	out << data.distancesPerMode;
	return out;
}

QDataStream &operator>>(QDataStream &in, ModeData &data)
{
	in >> data.distancesPerMode;
	return in;
}
