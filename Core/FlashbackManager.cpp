#include "FlashbackManager.h"
#include "UdpSpecification.h"

#include <QtDebug>

const constexpr int ECHO_DURATION_S = 18;

QStringList FlashbackManager::typeNames = {"Damage", "Penalty", "Retry", "Other"};

FlashbackManager::FlashbackManager() {}

void FlashbackManager::clear()
{
	counts.clear();
	_isFirstStatusSinceFlashback = false;
	_isFirstSessionSinceFlashback = false;
	_lastFlashbackCategorized = true;
}

void FlashbackManager::setLap(const LapData &lapData)
{
	if(flashbackDetected(lapData)) {
		// Flashback
		_lastFlashbackCategorized = false;

		if(lapData.m_penalties < _prevLapData.m_penalties ||
		   (!lapData.m_currentLapInvalid && _prevLapData.m_currentLapInvalid)) {
			addFlashback(FlashbackType::Penalty);
		}

		_isFirstStatusSinceFlashback = true;
		_isFirstSessionSinceFlashback = true;
	} else if(!_lastFlashbackCategorized && !_isFirstStatusSinceFlashback && !_isFirstSessionSinceFlashback) {
		addFlashback(FlashbackType::Other);
	}

	_isOnFinishLine = lapData.m_lapDistance > 0 &&
					  (lapData.m_lapDistance > _prevLapData.m_lapDistance + 50 || _prevLapData.m_lapDistance < 0);

	_prevLapData = lapData;
}

void FlashbackManager::setStatus(const CarStatusData &data)
{
	if(isFirstStatusSinceFlashback() && !_lastFlashbackCategorized) {
		if(data.m_frontLeftWingDamage < _prevStatusData.m_frontLeftWingDamage ||
		   data.m_frontRightWingDamage < _prevStatusData.m_frontRightWingDamage) {
			addFlashback(FlashbackType::Crash);
		}
	}

	if(_isFirstStatusSinceFlashback) {
		_isFirstStatusSinceFlashback = false;
	}

	_prevStatusData = data;
}

void FlashbackManager::setSession(const PacketSessionData &data)
{
	if(isFirstSessionSinceFlashback() && !_lastFlashbackCategorized) {
		auto echoDiff = data.m_sessionTimeLeft - _lastFlashbackSessionTime;
		if(_lastFlashbackSessionTime > 0 && qAbs(echoDiff) < ECHO_DURATION_S) {
			addFlashback(FlashbackType::Echo);
		}

		_lastFlashbackSessionTime = data.m_sessionTimeLeft;
	}

	if(_isFirstSessionSinceFlashback) {
		_isFirstSessionSinceFlashback = false;
	}
	_prevSessionData = data;
}

bool FlashbackManager::flashbackDetected(const LapData &data) const
{
	return _prevLapData.isValid() && data.m_totalDistance + 0.5 < _prevLapData.m_totalDistance &&
		   data.m_driverStatus > 0;
}

bool FlashbackManager::isOnFinishLine() const { return _isOnFinishLine; }

bool FlashbackManager::isFirstStatusSinceFlashback() const { return _isFirstStatusSinceFlashback; }

bool FlashbackManager::isFirstSessionSinceFlashback() const { return _isFirstSessionSinceFlashback; }

int FlashbackManager::totalCount() const
{
	int total = 0;
	for(const auto value : counts) {
		total += value;
	}

	return total;
}

void FlashbackManager::addFlashback(FlashbackType type)
{
	if(_lastFlashbackCategorized) {
		return;
	}

	if(!counts.contains(type)) {
		counts[type] = 0;
	}

	counts[type] += 1;
	_lastFlashbackCategorized = true;
}
