#ifndef STINT_H
#define STINT_H

#include <QDateTime>
#include <QStringList>
#include <QVector>

#include "Lap.h"
#include "Tyres.h"
#include "UdpSpecification.h"


class Stint : public Lap
{
  public:
	Stint(const QVector<TelemetryInfo> &dataInfo = {});

	QString description() const;

	int nbLaps() const;

	// Saving - Loading
	static Stint *fromFile(const QString &filename);

	QVector<float> lapTimes;
	TyresData<double> calculatedTyreWear;

  protected:
	virtual void saveData(QDataStream &out) const;
	virtual void loadData(QDataStream &in);
	virtual QVariantMap exportData() const;
};

#endif // STINT_H
